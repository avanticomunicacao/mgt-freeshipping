/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/totals'
], function (Component, totals) {
    'use strict';
    return Component.extend({
        isLoading: totals.isLoading,
        getInfosFreeShipping: function () {
            return {
                isModuleEnabled: window.checkoutConfig.avanti_free.isModuleEnabled,
                valueRemaining: window.checkoutConfig.avanti_free.valueRemaining,
                percentValue: window.checkoutConfig.avanti_free.percentValue,
                percentRemaining: window.checkoutConfig.avanti_free.percentRemaining
            };
        }
    });
});
