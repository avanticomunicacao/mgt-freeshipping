<?php

namespace Avanti\FreeShipping\ViewModel;

use Avanti\FreeShipping\Helper\Data as AvantiHelper;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class FreeShipping implements ArgumentInterface
{
    const PATH_CONFIG = 'freeshipping/general';

    protected $helper;

    public function __construct(AvantiHelper $helper)
    {
        $this->helper = $helper;
    }

    public function getInfos()
    {
        return $this->helper->valuesData();
    }

    public function moduleEnable()
    {
        return $this->helper->getConfigStore(self::PATH_CONFIG .'/enable');
    }

    public function modulePoupupEnable()
    {
        return $this->helper->getConfigStore(self::PATH_CONFIG . '/enable_modal');
    }
}