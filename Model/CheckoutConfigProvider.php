<?php

namespace Avanti\FreeShipping\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Avanti\FreeShipping\Helper\Data as AvantiHelper;

class CheckoutConfigProvider implements ConfigProviderInterface
{
    protected $helper;

    public function __construct(AvantiHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $data = $this->helper->valuesData();
        $result['avanti_free'] = $data;

        return $result;
    }
}