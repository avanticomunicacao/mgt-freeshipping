<?php

namespace Avanti\FreeShipping\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Cart;
use Magento\Store\Model\ScopeInterface;
use Magento\Directory\Model\Currency;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $cart;
    protected $currency;


    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Cart $cart,
        Currency $currency)
    {
        $this->scopeConfig = $scopeConfig;
        $this->cart = $cart;
        $this->currency = $currency;
    }

    public function valuesData($result = null)
    {
        if (!$this->moduleEnable()) {
            $result['isModuleEnabled'] = false;
            return $result;
        }
        $minimumOrderAmount = $this->getMinimumOrderAmount();
        $subTotalAmount = $result['subtotalAmount'] ? $result['subtotalAmount'] : $this->getGrandTotal();
        $amountRest = $minimumOrderAmount - $subTotalAmount;

        if ($amountRest < 0) {
            $result['valueRemaining'] = 'R$ 0,00';
            $result['percentValue'] = '100%';
            $result['percentRemaining'] = '0%';
            $result['isModuleEnabled'] = true;
            return $result;
        }

        $result['valueRemaining'] = $this->currency->format($amountRest, [], false, false);
        $percent = $this->calculatePercent($minimumOrderAmount, $subTotalAmount);
        $result['percentValue'] = $percent . '%';
        $percentRemaining = 100 - $percent;
        $result['percentRemaining'] = $percentRemaining . '%';
        $result['isModuleEnabled'] = true;

        return $result;
    }

    public function getMinimumOrderAmount()
    {
        return $this->scopeConfig->getValue("freeshipping/general/minimum_order_amount", ScopeInterface::SCOPE_STORES);
    }

    private function calculatePercent($valueFreeShipping, $subTotal)
    {
        $percent = $subTotal * 100 / $valueFreeShipping;
        return number_format($percent, 2,'.', ' ');
    }

    private function getGrandTotal()
    {
        return $this->cart->getQuote()->getSubtotal();
    }

    public function moduleEnable()
    {
        return $this->scopeConfig->getValue("freeshipping/general/enable", ScopeInterface::SCOPE_STORES);
    }

    public function getConfigStore($configPath)
    {
        return $this->scopeConfig->getValue($configPath, ScopeInterface::SCOPE_STORES);
    }
}