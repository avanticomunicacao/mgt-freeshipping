<?php

namespace Avanti\FreeShipping\Block\Catalog\Product\View;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Modal extends Template
{
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }
}
