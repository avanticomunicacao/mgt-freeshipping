<?php

namespace Avanti\FreeShipping\Block\Cart;

use Avanti\FreeShipping\Helper\Data as AvantiHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;

class FreeShippingBar extends Template
{
    protected $helper;

    public function __construct(Context $context, AvantiHelper $helper)
    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function getAmount()
    {
        return $this->helper->getMinimumOrderAmount();
    }
}