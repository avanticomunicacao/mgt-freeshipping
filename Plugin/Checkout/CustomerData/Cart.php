<?php

namespace Avanti\FreeShipping\Plugin\Checkout\CustomerData;

use Avanti\FreeShipping\Helper\Data as AvantiHelper;
use Magento\Checkout\CustomerData\Cart as CustomerDataCart;

class Cart
{
    protected $helper;

    public function __construct(AvantiHelper $helper)
    {
        $this->helper = $helper;
    }

    public function afterGetSectionData(CustomerDataCart $subject, array $result)
    {
       return $this->helper->valuesData($result);
    }
}