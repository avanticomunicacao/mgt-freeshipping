<?php

namespace Avanti\FreeShipping\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Catalog\Model\ProductRepository;
use Avanti\FreeShipping\Helper\Data as AvantiHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Catalog\Helper\Image as CatalogHelperImage;

class Index extends Action
{
    private $productRepository;
    private $helper;
    private $storeManager;
    private $catalogHelperImage;

    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        AvantiHelper $helper,
        StoreManagerInterface $storeManager,
        CatalogHelperImage $catalogHelperImage
    ) {
        $this->productRepository = $productRepository;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->catalogHelperImage = $catalogHelperImage;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }
        try {
            if (!$this->helper->moduleEnable()) {
                $response = $this->resultFactory
                    ->create(ResultFactory::TYPE_JSON)
                    ->setData(['module_disable' => 'true']);
            } else {
                $productId = $post['productId'];
                $product = $this->productRepository->getById($productId);
                $values = $this->helper->valuesData();

                $store = $this->storeManager->getStore();
                $productThumb = null;

                if ($product->getData('thumbnail')) {
                    $productThumb = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getData('thumbnail');
                } else {
                    $productThumb = $this->catalogHelperImage->getDefaultPlaceholderUrl('thumbnail');
                }

                $response = $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                    'productName' => $product->getName(),
                    'productPrice' => number_format($product->getPrice(), "2", ".", " "),
                    'valueRemaining' => $values['valueRemaining'],
                    'productThumbnail' => $productThumb,
                    'percentValue' => $values['percentValue'],
                    'percentRemaining' => $values['percentRemaining']
                ]);
            }
        } catch (\Exception $e) {
            throw new NotFoundException(__('Infos not found'));
        }
        return $response;
    }
}
