# Módulo FreeShipping
Esse módulo adiciona a funcionalidade de exibir quanto falta para o cliente possuir frete grátis.
Também adiciona um modal ao adicionar o produto no carrinho.

## Instalação
Adicione o seguinte código no node repositories do
composer.json do projeto:

```
"module-freeshipping": {
    "url": "https://bitbucket.org/avanticomunicacao/mgt-freeshipping.git",
    "type": "git"
}
```
Depois adicione o seguinte trecho no node require do composer.json

```"penseavanti/module-freeshipping": "1.0.5"```

Rode composer install para que a instalação seja feita.

## Configurações
As configurações do módulo estão disponíveis em:
**Store -> Configuration -> Avanti -> Free Shipping**.
As configurações disponíveis são:

1. **Enable**: Habilita ou Desabilita o módulo
2. **Enable Modal**: Habilita o Modal ao adicionar o produto ao carrinho
3 **Minimum Order Amount**:Valor mínimo informativo de frete grátis. (não está relacionado
a regras do Magento. O valor é somente informativo)

## Uso
Ao Configurar o módulo conforme desejado, adicione um produto ao
carrinho, e se a configuração de exibir o modal estiver ativa,
será exibido um alert com as informações de quanto falta para
chegar no valor mínimo para frete grátis.

Mas a principal funcionalidade do módulo, é exibit no **minicart**,**carrinho** 
e no **checkout** qual o valor necessário para atingir o frete grátis. (Valor informado nas configurações
do módulo lá no admin, conforme demonstrado logo acima.). 

Após configurar o valor necessário para atingir o frete grátis, e adicionar um produto ao carrinho,
A barra de frete grátis começa a ser exibida no **minicart**, **carrinho** e no **checkout**, conforme
imagens abaixo:

<strong>MiniCart:</strong>

![Image MiniCart FreeShipping](docs/minicart_freeshipping.png)

<strong>Carrinho:</strong>

![Image Cart Free Shipping](docs/cart_freeshipping.png)

<strong>Checkout</strong>

![Image Checkout Free Shipping](docs/checkout_freeshipping.png)